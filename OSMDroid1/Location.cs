﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OSMDroid1
{
    //erstellt von Stefan Stöhr
    class Location
    {
        private int id;
        private double cord_x;
        private double cord_y;
        private String name;

        public Location()
        {
            //leerer Konstruktor
        }

        public Location(int id, double cord_x, double cord_y, String name)
        {
            this.id = id;
            this.cord_x = cord_x;
            this.cord_y = cord_y;
            this.name = name;
        }

        //Getter
        public int getID()
        {
            return this.id;
        }
        public double getCord_x()
        {
            return this.cord_x;
        }
        public double getCord_y()
        {
            return this.cord_y;
        }
        public String getName()
        {
            return this.name;
        }

        //Setter
        public void setID(int id)
        {
            this.id = id;
        }
        public void setCord_x(double cord_x)
        {
            this.cord_x = cord_x;
        }
        public void setCord_y(double cord_y)
        {
            this.cord_y = cord_y;
        }
        public void setName(String name)
        {
            this.name = name;
        }

        //toString override
        public override string ToString()
        {
            return "(" + this.id + " | " + this.cord_x + " | " + this.cord_y + " | " + this.name + ")";
        }
    }
}
