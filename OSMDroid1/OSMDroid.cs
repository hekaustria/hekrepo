using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Collections;
using Osmdroid;
using Osmdroid.TileProvider.TileSource;
using Osmdroid.Util;
using Osmdroid.Views;
using Osmdroid.TileProvider.Util;
using Osmdroid.TileProvider;
using Osmdroid.TileProvider.Modules;
using Osmdroid.Api;
using Android.Views.InputMethods;
using Osmdroid.Views.Overlay;
using Osmdroid.Events;
using Android.Locations;
using System.Text.RegularExpressions;
using System.Drawing;


/*
 * Erstellt von Daniel Polzhofer
 * */

namespace OSMDroid1
{
    [Activity(Label = "PSS Maps", LaunchMode = Android.Content.PM.LaunchMode.SingleTop, ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]
    [IntentFilter(new string[] { "android.intent.action.SEARCH" })]
    [MetaData(("android.app.searchable"), Resource = "@xml/searchable")]

    public class Map : Activity, ILocationListener, ItemizedIconOverlay.IOnItemGestureListener
    {
        private string filename;
        private MapView mapView;
        private ListView listView;
        private ImageButton imageButton;
        private RelativeLayout.LayoutParams portrait;
        private RelativeLayout.LayoutParams landscape;
        LocationManager locMgr;
        String locationProvider;


        private int max_zoom;
        private int min_zoom;

        private String dir = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
        public static bool overlay;
        public bool gpsOverlay;
        public bool gpszoom;
        public bool button = false;


       
        private LocationDAO locDAO;
        private MetadataDAO metaDAO;
        private OverlayEventHandler oev;
        private Bounds bounds;
        private List<MapOverlay> overlayList;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            overlay = false;
            gpsOverlay = false;
            RelativeLayout relativeL = new RelativeLayout(this); 


            imageButton = new ImageButton(this);

            filename = Intent.GetStringExtra("Filename");
            DefaultResourceProxyImpl resProxy = new DefaultResourceProxyImpl(this.ApplicationContext);
            SimpleRegisterReceiver sr = new SimpleRegisterReceiver(this);

            Java.IO.File file = new Java.IO.File(dir + "/mbtiles", filename);

            IArchiveFile[] files = { MBTilesFileArchive.GetDatabaseFileArchive(file) };
            MapTileModuleProviderBase moduleProvider = new MapTileFileArchiveProvider(sr, TileSourceFactory.DefaultTileSource, files);
            MapTileModuleProviderBase[] pBaseArray = new MapTileModuleProviderBase[] { moduleProvider };
            MapTileProviderArray provider = new MapTileProviderArray(TileSourceFactory.DefaultTileSource, null, pBaseArray);
            mapView = new MapView(this, 256, resProxy, provider);
            //Datenbanken initalisieren
            initDatabase();

            SetMinMaxZoom();

            overlayList = new List<MapOverlay>();

            
         
            mapView.SetBuiltInZoomControls(true);
            mapView.SetMultiTouchControls(true);

            HandleIntent(Intent);

            ScaleBarOverlay myScaleBarOverlay = new ScaleBarOverlay(this);
            mapView.Overlays.Add(myScaleBarOverlay);
           

            oev = new OverlayEventHandler(this);
            mapView.OverlayManager.Add(1, oev);


            
            imageButton.SetImageResource(Resource.Drawable.ic_location);
            imageButton.Enabled = true;
            imageButton.SetBackgroundResource(Resource.Drawable.location);
            imageButton.SetScaleType(ImageView.ScaleType.FitCenter);
            imageButton.Click += delegate
            {
                button = !button;
                if (button == true)
                {
                    this.InitializeLocationManager();
                }
                else
                {
                    if (this.gpsOverlay != true)
                    {
                        imageButton.SetImageResource(Resource.Drawable.ic_location);
                        mapView.Invalidate();
                    }
                    else
                    {
                        imageButton.SetImageResource(Resource.Drawable.ic_location);
                        foreach (var mapoverlay in overlayList)
                        {
                            if (mapoverlay.GetOverlayName().Equals("GPS"))
                            {
                                mapView.OverlayManager.Remove(mapoverlay.GetItemizedIconOverlay());
                                overlayList.Remove(mapoverlay);
                                mapView.Invalidate();
                                break;
                               
                            }
                            
                        }
                        mapView.Invalidate();
                        gpsOverlay = false; 
                    }

                }

            };
            //ListView Design f�r Suche
            RelativeLayout.LayoutParams listParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MatchParent, RelativeLayout.LayoutParams.MatchParent);
            listView = new ListView(this);
            listView.Visibility = ViewStates.Invisible;
            listView.SetBackgroundColor(Android.Graphics.Color.ParseColor("#88676767"));



            portrait = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WrapContent, RelativeLayout.LayoutParams.WrapContent);
            portrait.Height = 130;
            portrait.Width = 130;


            landscape = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WrapContent, RelativeLayout.LayoutParams.WrapContent);
            landscape.Height = 130;
            landscape.Width = 130;


            var surfaceOrientation = WindowManager.DefaultDisplay.Rotation;

            if (surfaceOrientation == SurfaceOrientation.Rotation0 || surfaceOrientation == SurfaceOrientation.Rotation180)
            {
                var metrics = Resources.DisplayMetrics;
                var width = metrics.WidthPixels;
                var height = metrics.WidthPixels;

                portrait.LeftMargin = (width - portrait.Width);
                imageButton.LayoutParameters = portrait;
            }
            else
            {
                var metrics = Resources.DisplayMetrics;
                var width = metrics.WidthPixels;
                var height = metrics.WidthPixels;

                landscape.LeftMargin = (width - portrait.Width);
                imageButton.LayoutParameters = landscape;
            }


            //Views werden zum Layout hinzugef�gt
            relativeL.AddView(mapView);
            relativeL.AddView(imageButton);
            relativeL.AddView(listView);

            this.SetContentView(relativeL);
        }


        public void SetMinMaxZoom() // Min/Max Zoom und Karte position sezten
        {
            Metadata meta = new Metadata();

            this.max_zoom = metaDAO.getMaxZoomLevel();
            this.min_zoom = metaDAO.getMinZoomLevel();

            mapView.SetMaxZoomLevel(max_zoom);
            mapView.SetMinZoomLevel(min_zoom);

            mapView.SetUseDataConnection(false);
            bounds = metaDAO.getBounds();
            try
            {
                GeoPoint gp = bounds.getCenter();
                IMapController mapController = mapView.Controller;
                mapController.SetZoom(max_zoom - 2);
                mapController.SetCenter(gp);
                
                mapView.Invalidate();

                GeoPoint gp2 = new GeoPoint(bounds.getGeoPoint("point_A").Latitude - 0.005, bounds.getGeoPoint("point_A").Longitude - 0.05);
                GeoPoint gp3 = new GeoPoint(bounds.getGeoPoint("point_B").Latitude + 0.005, bounds.getGeoPoint("point_B").Longitude + 0.055);

                List<GeoPoint> l = new List<GeoPoint>();
                l.Add(gp2);
                l.Add(gp3);
              
                mapView.ScrollableAreaLimit = BoundingBoxE6.FromGeoPoints(l);

            }
            catch (Exception)
            {
                Android.App.AlertDialog.Builder builder = new Android.App.AlertDialog.Builder(this);
                Android.App.AlertDialog dialog = builder.Create();
                dialog.SetTitle("Achtung!");
                dialog.SetMessage("File ist fehlerhaft");

                dialog.SetButton2("OK", (sender2, e2) =>
                {
                    OnBackPressed();

                });
                dialog.Show(); ;
            }

        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_search, menu);

            var searchManager = (SearchManager)GetSystemService(Context.SearchService);
            var searchView = (SearchView)menu.FindItem(Resource.Id.options_menu_search).ActionView;

            searchView.SetSearchableInfo(searchManager.GetSearchableInfo(ComponentName));
            searchView.SetIconifiedByDefault(false);


            Java.IO.File mbFolder = new Java.IO.File("sdcard/", "overlay");
            
            if(mbFolder.Exists() == true)
            {
                if(mbFolder.ListFiles().Count() > 0 )
                {
                    foreach (var file in mbFolder.ListFiles())
                    {

                        IMenuItem mi = menu.Add(file.Name.Replace(".mbtiles", ""));
                        mi.SetCheckable(true);
                    }
                }
                else
                {
                        IMenuItem mi = menu.Add("kein Overlay vorhanden");
                }
                
                
            }
            else
            {
                  mbFolder.Mkdir();
            }
            
            
           
            
            return true;

        }



        protected override void OnNewIntent(Intent intent)
        {
            HandleIntent(intent);
        }


        private void HandleIntent(Intent intent)
        {
            if (Intent.ActionSearch.Equals(intent.Action))
            {
                string query = intent.GetStringExtra(SearchManager.Query).Trim();
                this.SearchLocation(query);
            }
        }

        //Location Suchmethode
        public void SearchLocation(String query)
        {
            listView.Visibility = ViewStates.Gone;
            List<Location> locList = locDAO.getLocationByName(query);
            Location loc;

            if (locList != null)
            {

                if (locList.Count <= 1)
                {
                    loc = locList[0];
                    SetPositon(loc);
                }
                else
                {
                    listView.Adapter = new ArrayAdapter<Location>(this, Android.Resource.Layout.SimpleListItem1, locList);
                    listView.Visibility = ViewStates.Visible;
                    listView.ItemClick += (sender, e) => { SetPositon(sender, e, locList); };

                }
            }
            else
            {
                Toast.MakeText(this, "Keine Ergebnisse: " + query, ToastLength.Short).Show();
            }

        }

        //Position des Gesuchten Objekts setzen (bei meheren Objekten)
        private void SetPositon(object sender, AdapterView.ItemClickEventArgs e, List<Location> locList)
        {

            Location loc = locList[e.Position];
            listView.Visibility = ViewStates.Gone;

            GeoPoint geoPoint = new GeoPoint(loc.getCord_x(), loc.getCord_y());
            /* IMapController mapController = mapView.Controller;
             mapController.SetZoom(18);
             mapController.AnimateTo(geoPoint);
             ArrayList anotherOverlayItemArray = new ArrayList();
             anotherOverlayItemArray.Add(new OverlayItem(loc.getName(), "", geoPoint));

             ItemizedIconOverlay anotherItemizedIconOverlay = new ItemizedIconOverlay(this, anotherOverlayItemArray, this);
             if (overlay != true)
             {
                 overlay = true;

             }
             else
             {
                 mapView.OverlayManager.Remove(1);
             }
             mapView.OverlayManager.Add(1, anotherItemizedIconOverlay);
             mapView.Invalidate();*/

            oev.SetPoint(geoPoint, mapView);

        }

        //Position des Gesuchten Objekts setzen (falls eindeutig)
        private void SetPositon(Location loc)
        {

            listView.Visibility = ViewStates.Gone;

            GeoPoint geoPoint = new GeoPoint(loc.getCord_x(), loc.getCord_y());
            oev.SetPoint(geoPoint, mapView);
            /* IMapController mapController = mapView.Controller;
             mapController.SetZoom(18);
             mapController.AnimateTo(geoPoint);
             ArrayList anotherOverlayItemArray = new ArrayList();
             anotherOverlayItemArray.Clear();
             anotherOverlayItemArray.Add(new OverlayItem(loc.getName(), "", geoPoint));

             ItemizedIconOverlay anotherItemizedIconOverlay = new ItemizedIconOverlay(this, anotherOverlayItemArray, this);
             if (overlay != true)
             {
                 overlay = true;

             }
             else
             {
                 mapView.OverlayManager.Remove(1);
             }
             mapView.OverlayManager.Add(1, anotherItemizedIconOverlay);
             mapView.Invalidate();*/

        }

        //GPS

        protected override void OnResume()
        {
            base.OnResume();

        }

        public void InitializeLocationManager()
        {
            /*  locMgr = GetSystemService(Context.LocationService) as LocationManager;

              locationProvider = LocationManager.GpsProvider;*/

            Criteria locationCriteria = new Criteria();
            locMgr = GetSystemService(Context.LocationService) as LocationManager;
            locationCriteria.Accuracy = Accuracy.Coarse;
            locationCriteria.PowerRequirement = Power.Medium;
            locationProvider = locMgr.GetBestProvider(locationCriteria, true);


            if (locMgr.IsProviderEnabled(LocationManager.GpsProvider))
            {
                imageButton.SetImageResource(Resource.Drawable.ic_location_active);
                if (locationProvider != null)
                {
                    locMgr.RequestLocationUpdates(locationProvider, 0, 0, this);
                }
            }
            else
            {
                //Toast.MakeText(this,"Fail",ToastLength.Short).Show();
                Android.App.AlertDialog.Builder builder = new Android.App.AlertDialog.Builder(this);
                Android.App.AlertDialog alertDialog = builder.Create();
                alertDialog.SetTitle("Standortdienste deaktiviert");
                alertDialog.SetMessage("Maps ben�tigt Zugriff auf Ihren Standort. Aktivieren Sie den Standordzugriff.");
                alertDialog.SetButton("Einstellungen", (sender, e) =>
                {
                    Intent intent = new Intent(Android.Provider.Settings.ActionLocationSourceSettings);
                    StartActivity(intent);
                });
                alertDialog.SetButton2("Ingnorieren", (sender, e) => { });
                alertDialog.Show();
            }
        }



        public void OnLocationChanged(Android.Locations.Location location)
        {
            if (button == true)
            {
                GeoPoint gp = new GeoPoint(location.Latitude, location.Longitude);
                
                
               // if ((gp.Latitude > bounds.getGeoPoint("point_A").Latitude && gp.Longitude > bounds.getGeoPoint("point_A").Longitude) || (gp.Latitude < bounds.getGeoPoint("point_B").Latitude && gp.Longitude < bounds.getGeoPoint("point_B").Longitude))
                if (gp.Latitude > bounds.getGeoPoint("point_A").Latitude || gp.Latitude > bounds.getGeoPoint("point_B").Latitude || gp.Longitude > bounds.getGeoPoint("point_A").Longitude || gp.Longitude > bounds.getGeoPoint("point_B").Longitude)
                {
                  
            
                    ZoomToGPSPosition(gp);

                    gpszoom = true;
                    List<OverlayItem> itemList = new List<OverlayItem>();
                    OverlayItem olItem = new OverlayItem("GPS", "Position", gp);
                    itemList.Clear();
                    
                    var newMarker = this.Resources.GetDrawable(Resource.Drawable.blue_dot);
                    
                    olItem.SetMarker(newMarker);
                    itemList.Add(olItem);

                    if (this.gpsOverlay != true)
                    {
                        this.gpsOverlay = true;

                    }
                    else
                    {
                        foreach (var mapoverlay in overlayList)
                        {
                            if (mapoverlay.GetOverlayName().Equals("GPS"))
                            {
                                mapView.OverlayManager.Remove(mapoverlay.GetItemizedIconOverlay());
                                overlayList.Remove(mapoverlay);
                                mapView.Invalidate();
                                break;
                                
                            }
                        }
                    }
                    ItemizedIconOverlay gpsOverlay = new ItemizedIconOverlay(this, itemList, this);
                    mapView.OverlayManager.Add(2, gpsOverlay);
                    overlayList.Add(new MapOverlay("GPS", gpsOverlay));
                    mapView.Invalidate();
                
                }
                else
                {
                    Toast.MakeText(this,"Ihre Position ist au�erhalb der Karte",ToastLength.Long).Show();
                }
            }

        }

        public void ZoomToGPSPosition(GeoPoint gp)
        {
            if (gpszoom == true)
            {
                IMapController mapController = mapView.Controller;
                mapController.SetZoom(this.max_zoom);
                mapController.SetCenter(gp);
                gpszoom = false;
            }

        }

        public void OnProviderDisabled(string provider)
        {
            throw new NotImplementedException();
        }

        public void OnProviderEnabled(string provider)
        {
            throw new NotImplementedException();
        }

        public void OnStatusChanged(string provider, Availability status, Bundle extras)
        {
            throw new NotImplementedException();
        }

        public void Navigation(GeoPoint gp)
        {
            Intent intent = new Intent(Android.Content.Intent.ActionView, Android.Net.Uri.Parse("http://maps.google.com/maps?&daddr=" + gp.Latitude.ToString().Replace(",", ".") + "," + gp.Longitude.ToString().Replace(",", ".")));
            StartActivity(intent);
        }

        public bool OnItemLongPress(int index, Java.Lang.Object item)
        {
            //return false;
            /*
             * Hier die Funktion zum Navigieren einf�gen
             */
          

            return true;
        }

        public bool OnItemSingleTapUp(int index, Java.Lang.Object item)
        {
            /*var overlayItem = item as OverlayItem;
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.SetMessage("Wollen Sie hierher Navigieren?");
            /*dialog.SetPositiveButton("Nein", (sender, e) =>
            {
                continue;
            });
            dialog.SetNegativeButton("Ja", (sender, e) =>
            {
                Navigation(overlayItem.Point);
            });
            dialog.Show();
            dialog.SetPositiveButton("Nein", (sender, e) =>
            {
                
            });
            dialog.SetNegativeButton("Ja", (sender, e) =>
            {
                //continue;
                Navigation(overlayItem.Point);
            });
            dialog.Show();*/

            return true;

        }

        public override void OnConfigurationChanged(Android.Content.Res.Configuration newConfig)
        {
            base.OnConfigurationChanged(newConfig);

            var metrics = Resources.DisplayMetrics;
            var width = metrics.WidthPixels;
            var height = metrics.WidthPixels;

            if (newConfig.Orientation == Android.Content.Res.Orientation.Portrait)
            {
                portrait.LeftMargin = (width - portrait.Width);
                imageButton.LayoutParameters = portrait;
            }
            else if (newConfig.Orientation == Android.Content.Res.Orientation.Landscape)
            {
                landscape.LeftMargin = (width - landscape.Width);
                imageButton.LayoutParameters = landscape;
            }

        }

        private void initDatabase()
        {
            //Datenbank initalisieren
            locDAO = new LocationDAO(dir + "/mbtiles/" + filename);
            metaDAO = new MetadataDAO(dir + "/mbtiles/" + filename);
        }


        public override void OnBackPressed()
        {
            locMgr = null;
            base.OnBackPressed();
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            bool check = item.IsChecked;
            check = !check;
          
            item.SetChecked(check);

            if(item.IsChecked == true)
            {
                try
                {
                    DefaultResourceProxyImpl resProxy = new DefaultResourceProxyImpl(this.ApplicationContext);
                    SimpleRegisterReceiver sr = new SimpleRegisterReceiver(this);
                    Java.IO.File file2 = new Java.IO.File("sdcard/overlay/" + item.ToString() + ".mbtiles");
                    IArchiveFile[] files2 = { MBTilesFileArchive.GetDatabaseFileArchive(file2) };
                    MapTileModuleProviderBase moduleProvider2 = new MapTileFileArchiveProvider(sr, TileSourceFactory.BaseOverlayNl, files2);
                    MapTileModuleProviderBase[] pBaseArray2 = new MapTileModuleProviderBase[] { moduleProvider2 };
                    MapTileProviderArray provider2 = new MapTileProviderArray(TileSourceFactory.BaseOverlayNl, null, pBaseArray2);
                    // MapView mapView2 = new MapView(this, 256, resProxy, provider2);
                    TilesOverlay to = new TilesOverlay(provider2, this);
                    to.LoadingBackgroundColor = TilesOverlay.MenuMapMode;
                    mapView.OverlayManager.Add(2, to);
                    mapView.Invalidate();
                    overlayList.Add(new MapOverlay(item.ToString(), to));
                    
                }
                catch (Exception)
                {
                    Toast.MakeText(this, "Karte nicht vorhanden", ToastLength.Long).Show();
                }
            }
            else
            {
                try
                {
                    foreach(var mapoverlay in overlayList)
                    {
                        if(item.ToString().Equals(mapoverlay.GetOverlayName()))
                        {                          
                            mapView.OverlayManager.Remove(mapoverlay.GetTileOverlay());
                            mapView.Invalidate();
                            overlayList.Remove(mapoverlay);
                            break;
                        }
                    }
                    
                }
                catch (Exception)
                {
                    
                    
                }
               
            }
            return true;

        }
       
    }

}



