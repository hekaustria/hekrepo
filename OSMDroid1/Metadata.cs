using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace OSMDroid1
{
    //erstellt von Stefan St�hr

    class Metadata
    {
        private String name;
        private String value;

        public Metadata()
        {

        }

        public Metadata(String name, String value)
        {
            this.name = name;
            this.value = value;
        }
        public String getName()
        {
            return this.name;
        }
        public String getValue()
        {
            return this.value;
        }
        public void setName(String name)
        {
            this.name = name;
        }
        public void setValue(String value)
        {
            this.value = value;
        }
        public override string ToString()
        {
            return "(" + this.name + "|" + this.value + ")";
        }
    }
}

