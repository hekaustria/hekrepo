
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Java.Lang;
using Java.Util;

namespace OSMDroid1
{

    public class DownloadFragment : ListFragment
    {
        private ArrayList maps;
        private RequestService service = RequestService.getInstance();

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            if (this.ListAdapter == null)
            {

                //Load token (tenantId, user and pw are hardcoded for testing) 
                service.loadToken("", "", "");
                //Load maps and store it in adapter
                //var adapter = new ArrayAdapter(this.Activity, Android.Resource.Layout.SimpleListItem1, service.loadMapsInGeoCloud(token).ToArray());
                var adapter = new DownloadListAdapter(this.Activity); 
                adapter.updateList(service.loadMapsInGeoCloud(this.Activity));
                this.ListAdapter = adapter;

            }

        }

        public override View OnCreateView(LayoutInflater inflater,
       ViewGroup container, Bundle savedInstanceState)
        {
            var view = base.OnCreateView(inflater, container, savedInstanceState);
            return view;
        }

    }
}