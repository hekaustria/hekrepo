using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace OSMDroid1
{
    class DownloadImageException : Exception
    {
        public DownloadImageException() { }

        public DownloadImageException(String message) : base(message) { }
    }
}